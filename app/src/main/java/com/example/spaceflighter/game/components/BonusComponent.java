package com.example.spaceflighter.game.components;

public class BonusComponent {
    public int health_bonus = 0;
    public int gun_damage_up = 0;
    public int gun_bullets_up = 0;
    public int fireRate_up = 0;

    public BonusComponent() {}

    public BonusComponent(int health_bonus, int gun_damage_up, int gun_bullets_up, int fireRate_up) {
        this.health_bonus = health_bonus;
        this.gun_damage_up = gun_damage_up;
        this.gun_bullets_up = gun_bullets_up;
        this.fireRate_up = fireRate_up;
    }
}
