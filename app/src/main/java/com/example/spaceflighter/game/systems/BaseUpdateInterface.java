package com.example.spaceflighter.game.systems;

public interface BaseUpdateInterface {
    void update(long dT);
}
