package com.example.spaceflighter;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.spaceflighter.assets.Textures;
import com.example.spaceflighter.fragments.EndActivity;
import com.example.spaceflighter.fragments.mvp.GamePresenter;
import com.example.spaceflighter.fragments.mvp.GamePresenterViewInterface;
import com.example.spaceflighter.graphics.MyGLSurfaceView;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import moxy.MvpAppCompatActivity;
import moxy.presenter.InjectPresenter;

public class GameActivity extends MvpAppCompatActivity implements GamePresenterViewInterface {

    @InjectPresenter
    GamePresenter mPresenter;

    private MyGLSurfaceView mGLView;

    private final ActivityResultLauncher<Intent> mLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if(result != null) {
            boolean newGame = result.getData().getBooleanExtra("newGame",false);
            if (!newGame) finish();
            else mGLView.newGame();
        }
    });

    @Override
    public void onBackPressed() { finish(); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        mGLView = findViewById(R.id.myGLSurfaceView);

        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(visibility -> fullscreen());
        fullscreen();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGLView != null) {
            mGLView.onResume();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        if (mGLView != null) {
            mGLView.onPause();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        fullscreen();
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void onDestroy() {
        Textures.reset();
        super.onDestroy();
    }

    @Override
    public void runGame() {
        mGLView.setCallback(new MyGLSurfaceView.GameInterface() {
            @Override
            public void onEnd(long timeSurvived) {
                mPresenter.endGame(getApplicationContext(), timeSurvived);
            }

            @Override
            public void onTexturesLoaded() {
                hideLoadingScreen();
            }
        });
        mGLView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingScreen() {
        ConstraintLayout cl = findViewById(R.id.layout_loading_screen);
        cl.setVisibility(View.VISIBLE);
        TextView sub = findViewById(R.id.textView_loading);
        sub.setText(getApplicationContext().getResources().getString(R.string.msg_loading_textures));
    }

    @Override
    public void runEndScreen(long time) {
        Intent i = new Intent(this, EndActivity.class);
        Bundle b = new Bundle();
        b.putLong("time", time);
        i.putExtras(b);
        mLauncher.launch(new Intent(i));
    }

    @Override
    public void serverError() {
        Toast.makeText(this, getString(R.string.msg_server_error), Toast.LENGTH_LONG).show();
        new Handler().postDelayed(this::finish, 2000);
    }

    private void hideLoadingScreen() {
        ConstraintLayout cl = findViewById(R.id.layout_loading_screen);
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(() -> handler.post(() -> cl.setVisibility(View.GONE)));
    }

    private void fullscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}