package com.example.spaceflighter.graphics;

import android.content.Context;
import android.media.MediaPlayer;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.example.spaceflighter.R;
import com.example.spaceflighter.game.Game;
import com.example.spaceflighter.util.SharedPref;

public class MyGLSurfaceView extends GLSurfaceView {

    private MyGLRenderer renderer;
    private Game game;
    private GameInterface callback;
    private MediaPlayer mediaPlayer;

    public MyGLSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setEGLContextClientVersion(2);
        game = new Game();
        renderer = new MyGLRenderer(getContext(), game);
        setPreserveEGLContextOnPause(true);
        renderer.setRenderer(() -> callback.onTexturesLoaded());

        setRenderer(renderer);
        game.setCallback(new Game.GameCallback() {
            @Override
            public void endGame(long timeSurvived) {
                if(callback != null) callback.onEnd(timeSurvived);
            }

            @Override
            public void playSound(byte sound) {
                if(SharedPref.getSoundSettings(context)) {
                    switch (sound) {
                        case Game.SHOOT_SOUND:
                            mediaPlayer = MediaPlayer.create(context, R.raw.shot);
                            mediaPlayer.start();
                            break;
                        case Game.EXPLOSION_SOUND:
                            mediaPlayer = MediaPlayer.create(context, R.raw.explosion);
                            mediaPlayer.start();
                            break;
                    }
                }
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = event.getActionMasked();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                game.onTouch(event.getX(), event.getY());
                break;
        }
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
        game.pauseOn();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mediaPlayer != null) mediaPlayer.release();
        game.pauseOff();
    }

    public void setCallback(GameInterface callback) {
        this.callback = callback;
    }

    public void newGame() { game.newGame();}

    public interface GameInterface {
        void onEnd(long timeSurvived);
        void onTexturesLoaded();
    }

}
