package com.example.spaceflighter.fragments.mvp;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.OneExecution;
import moxy.viewstate.strategy.alias.SingleState;

public interface GamePresenterViewInterface extends MvpView {

    @SingleState
    void runGame();

    @OneExecution
    void showLoadingScreen();

    @OneExecution
    void runEndScreen(long time);

    @OneExecution
    void serverError();
}
