package com.example.spaceflighter.game.systems;

import com.example.spaceflighter.game.Entity;

import java.util.ArrayList;
import java.util.List;

public interface BaseSystem {

    List<String> neededComponents();

    void uploadEntities(ArrayList<Entity> entities);

    void addEntity(Entity entity);

    void removeEntity(Entity entity);
}
