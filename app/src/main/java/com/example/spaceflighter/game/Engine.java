package com.example.spaceflighter.game;

import com.example.spaceflighter.game.components.ExplosionComponent;
import com.example.spaceflighter.game.systems.BaseSystem;
import com.example.spaceflighter.game.systems.BaseUpdateInterface;
import com.example.spaceflighter.game.systems.ExplodionSystem;
import com.example.spaceflighter.game.systems.RenderUpdateInterface;

import java.util.ArrayList;
import java.util.List;

public class Engine {
    private ArrayList<Entity> entities = new ArrayList<>();
    private ArrayList<BaseSystem> systems = new ArrayList<>();

    public Engine() {}

    public void addEntity(Entity entity) {
        entities.add(entity);
        for(BaseSystem system: systems) {
            List<String> neededComponents = system.neededComponents();
            boolean add = true;
            for(String neededComponent: neededComponents) {
                if(entity.getComponent(neededComponent) == null) add = false;
            }
            if(add) system.addEntity(entity);
        }
    }

    public void addSystem(BaseSystem system) {
        List<String> neededComponents = system.neededComponents();
        ArrayList<Entity> outputEntities = new ArrayList<>();
        for (Entity entity : entities) {
            boolean add = true;
            for(String neededComponent: neededComponents) {
                if(entity.getComponent(neededComponent) == null) add = false;
            }
            if(add) outputEntities.add(entity);
        }
        system.uploadEntities(outputEntities);
        systems.add(system);
    }

    public void update(long dT, float[] mvpMatrix) {
        removeDeadEntities();
        for (BaseSystem system : systems) {
            if(system instanceof BaseUpdateInterface) ((BaseUpdateInterface) system).update(dT);
            if( system instanceof RenderUpdateInterface) ((RenderUpdateInterface) system).draw(mvpMatrix);
        }
    }

    public void clearEntities() {
        while(entities.size() > 0) {
            for(BaseSystem baseSystem: systems) baseSystem.removeEntity(entities.get(0));
            entities.remove(0);
        }
    }

    private void removeDeadEntities() {
        for(int i = 0; i < entities.size(); i++) {
            if(entities.get(i).isDead()) {
                for(BaseSystem baseSystem: systems) {
                    if(baseSystem instanceof ExplodionSystem &&
                            entities.get(i).getComponent(ExplosionComponent.class.getName()) != null) baseSystem.addEntity(entities.get(i));
                    else baseSystem.removeEntity(entities.get(i));
                }
                entities.remove(i);
                i--;
            }
        }
    }
}
