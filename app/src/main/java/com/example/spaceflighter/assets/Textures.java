package com.example.spaceflighter.assets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import androidx.appcompat.content.res.AppCompatResources;

import com.example.spaceflighter.R;
import com.example.spaceflighter.util.Util;

import java.util.HashMap;
import java.util.Map;

public class Textures {
    public static int SPACESHIP, SHOT_BLUE, ENEMY_1, ENEMY_2, ENEMY_3, BONUS_HEALTH, BONUS_GUN, HEART, EXPLOSION_SHEET, BACKGROUND_STAR;
    public static int[] numbers = new int[10];
    private static Map<String , Bitmap> bitmaps = new HashMap<>();
    public static Bitmap background;
    private static boolean texturesLoaded = false;

    public static int loadTexture(Context context, int resourceId) {
        int[] textureHandle = new int[1];

        GLES20.glGenTextures(1, textureHandle, 0);

        if (textureHandle[0] != 0) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;
            final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
            GLES20.glEnable(GLES20.GL_BLEND);

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
            bitmap.recycle();
        }

        if (textureHandle[0] == 0) {
            throw new RuntimeException("Error loading texture.");
        }

        return textureHandle[0];
    }

    public static int loadTexture(Bitmap bitmap) {
        int[] textureHandle = new int[1];

        GLES20.glGenTextures(1, textureHandle, 0);

        if (textureHandle[0] != 0) {
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
            GLES20.glEnable(GLES20.GL_BLEND);

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_REPEAT);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_REPEAT);

            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
        }

        if (textureHandle[0] == 0) {
            throw new RuntimeException("Error loading texture.");
        }

        return textureHandle[0];
    }

    private static boolean loaded = false;

    public static void load(Context context) {
        if (!loaded && texturesLoaded) {
            SPACESHIP = loadTexture(bitmaps.get("player"));
            SHOT_BLUE = loadTexture(bitmaps.get("shot"));
            ENEMY_1 = loadTexture(bitmaps.get("enemy_1"));
            ENEMY_2 = loadTexture(bitmaps.get("enemy_2"));
            ENEMY_3 = loadTexture(bitmaps.get("enemy_3"));
            Drawable health_bonus = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_add_circle_outline_24);
            if(health_bonus != null) BONUS_HEALTH = loadTexture(Util.svgToBitmap(health_bonus, 64, 64));
            Drawable gun_bonus = AppCompatResources.getDrawable(context,R.drawable.ic_sharp_keyboard_double_arrow_up_24);
            if(gun_bonus != null) BONUS_GUN = loadTexture(Util.svgToBitmap(gun_bonus, 64, 64));
            Drawable heart = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_favorite_24);
            if(heart != null) HEART = loadTexture(Util.svgToBitmap(heart, 64, 64));
            EXPLOSION_SHEET = loadTexture(bitmaps.get("explosion"));
            BACKGROUND_STAR = loadTexture(bitmaps.get("back"));
            numbers[0] = loadTexture(Util.createTxtImage("0", 20, Color.WHITE));
            numbers[1] = loadTexture(Util.createTxtImage("1", 20, Color.WHITE));
            numbers[2] = loadTexture(Util.createTxtImage("2", 20, Color.WHITE));
            numbers[3] = loadTexture(Util.createTxtImage("3", 20, Color.WHITE));
            numbers[4] = loadTexture(Util.createTxtImage("4", 20, Color.WHITE));
            numbers[5] = loadTexture(Util.createTxtImage("5", 20, Color.WHITE));
            numbers[6] = loadTexture(Util.createTxtImage("6", 20, Color.WHITE));
            numbers[7] = loadTexture(Util.createTxtImage("7", 20, Color.WHITE));
            numbers[8] = loadTexture(Util.createTxtImage("8", 20, Color.WHITE));
            numbers[9] = loadTexture(Util.createTxtImage("9", 20, Color.WHITE));
            loaded = true;
        }
    }

    public static boolean isLoaded() { return Textures.loaded;}

    public static void reset() {
        loaded = false;
    }

    public static void setBackground(Bitmap background) {
        Textures.background = background;
    }

    public static boolean isBackgroundLoaded() { return Textures.background != null;}

    public static boolean isTexturesDownloaded() { return texturesLoaded;}

    public static void setBitmaps(Bitmap[] bitmaps) {
        for (int i = 0; i < bitmaps.length; i++) {
            switch (i) {
                case 0:
                    Textures.bitmaps.put("player", bitmaps[0]);
                    break;
                case 1:
                    Textures.bitmaps.put("enemy_1", bitmaps[1]);
                    break;
                case 2:
                    Textures.bitmaps.put("enemy_2", bitmaps[2]);
                    break;
                case 3:
                    Textures.bitmaps.put("enemy_3", bitmaps[3]);
                    break;
                case 4:
                    Textures.bitmaps.put("shot", bitmaps[4]);
                    break;
                case 5:
                    Textures.bitmaps.put("back", bitmaps[5]);
                    break;
                case 6:
                    Textures.bitmaps.put("explosion", bitmaps[6]);
                    break;
            }
        }
        texturesLoaded = true;
    }
}
