package com.example.spaceflighter.game.systems;

import com.example.spaceflighter.game.Entity;
import com.example.spaceflighter.game.components.BoundsComponent;
import com.example.spaceflighter.game.components.CoordsComponent;
import com.example.spaceflighter.game.components.EnemyShootingComponent;

import java.util.ArrayList;
import java.util.List;

public class EnemyShootingSystem implements BaseSystem, BaseUpdateInterface {
    private ArrayList<Entity> entities = new ArrayList<>();
    private ShootCallback shootCallback;

    public EnemyShootingSystem(ShootCallback shootCallback) {
        this.shootCallback = shootCallback;
    }

    @Override
    public List<String> neededComponents() {
        List<String> output = new ArrayList<>();
        output.add(EnemyShootingComponent.class.getName());
        output.add(CoordsComponent.class.getName());
        output.add(BoundsComponent.class.getName());
        return output;
    }

    @Override
    public void uploadEntities(ArrayList<Entity> entities) {
        this.entities = entities;
    }

    @Override
    public void addEntity(Entity entity) {
        if(entity.getComponent(EnemyShootingComponent.class.getName()) != null &&
                entity.getComponent(CoordsComponent.class.getName()) != null &&
                entity.getComponent(BoundsComponent.class.getName()) != null) entities.add(entity);
    }

    @Override
    public void removeEntity(Entity entity) {
        entities.remove(entity);
    }

    @Override
    public void update(long dT) {
        EnemyShootingComponent component;
        CoordsComponent coordsComponent;
        BoundsComponent boundsComponent;
        for(Entity entity: entities) {
            component = (EnemyShootingComponent) entity.getComponent(EnemyShootingComponent.class.getName());
            coordsComponent = (CoordsComponent) entity.getComponent(CoordsComponent.class.getName());
            boundsComponent = (BoundsComponent) entity.getComponent(BoundsComponent.class.getName());
            component.currentShootInterval += dT;
            if(component.currentShootInterval > component.shootInterval) {
                component.currentShootInterval = 0;
                if(shootCallback != null) shootCallback.shoot(coordsComponent.posX, boundsComponent.bounds.bottom, component.damage);
            }
        }
    }

    public interface ShootCallback {
        void shoot(float x, float y, int damage);
    }
}
