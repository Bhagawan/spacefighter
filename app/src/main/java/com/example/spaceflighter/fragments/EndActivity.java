package com.example.spaceflighter.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.example.spaceflighter.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class EndActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end);
        fullscreen();

        Intent i = getIntent();
        if(i != null) {
            setRecord(i.getExtras().getLong("time"));
        } else finish();

        AppCompatButton newGameButton = findViewById(R.id.button_new_game);
        newGameButton.setOnClickListener(v -> exit(true));
        AppCompatButton exitButton = findViewById(R.id.button_end_exit);
        exitButton.setOnClickListener(v -> exit(false));
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        fullscreen();
        return super.dispatchTouchEvent(event);
    }

    public void setRecord(long time) {
        TextView scoreText = findViewById(R.id.textview_end_time);
        DateFormat formatter = new SimpleDateFormat("mm:ss", Locale.getDefault());
        scoreText.setText(formatter.format(new Date(time)));
    }

    private void exit(boolean newGame) {
        Intent i = new Intent();
        Bundle b = new Bundle();
        b.putBoolean("newGame", newGame);
        i.putExtras(b);
        setResult(RESULT_OK, i);
        finish();
    }

    private void fullscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}