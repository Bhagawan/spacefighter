package com.example.spaceflighter.game.components;

public class PlayerComponent {
    public int bulletsAmount = 1;
    public float bulletSpreadAngle = 0;
    public int bulletDamage = 10;
    public long fireRate = 1000; // 1 bullet every fireRate milliseconds

    public PlayerComponent(int bulletsAmount, float bulletSpreadAngle, int bulletDamage) {
        this.bulletsAmount = bulletsAmount;
        this.bulletSpreadAngle = bulletSpreadAngle;
        this.bulletDamage = bulletDamage;
    }

    public PlayerComponent(int bulletsAmount, float bulletSpreadAngle, int bulletDamage, int fireRate) {
        this.bulletsAmount = bulletsAmount;
        this.bulletSpreadAngle = bulletSpreadAngle;
        this.bulletDamage = bulletDamage;
        this.fireRate = fireRate;
    }


}
