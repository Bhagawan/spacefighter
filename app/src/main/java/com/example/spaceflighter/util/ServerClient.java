package com.example.spaceflighter.util;

import com.example.spaceflighter.data.SplashResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ServerClient {

    @FormUrlEncoded
    @POST("SpaceFighter/splash.php")
    Call<SplashResponse> getSplash(@Field("locale") String locale);

    @GET("SpaceFighter/Assets/{file}")
    Call<ResponseBody> getBitmap(@Path("file") String file);
}
