package com.example.spaceflighter.game.components;

public class HealthComponent {
    public int health;

    public HealthComponent() {}

    public HealthComponent(int health) {
        this.health = health;
    }
}
