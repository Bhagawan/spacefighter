package com.example.spaceflighter.game.components;

public class MovingComponent {
    public float vectorX, vectorY;
    public float speed;

    public MovingComponent() {}

    public MovingComponent(float vectorX, float vectorY, float speed) {
        this.vectorX = vectorX;
        this.vectorY = vectorY;
        this.speed = speed;
    }

    public void setVector(float vectorX, float vectorY) {
        this.vectorX = vectorX;
        this.vectorY = vectorY;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }
}
