package com.example.spaceflighter.game.systems;

import com.example.spaceflighter.game.Entity;
import com.example.spaceflighter.game.components.CoordsComponent;
import com.example.spaceflighter.game.components.MovingComponent;
import com.example.spaceflighter.game.components.PathComponent;

import java.util.ArrayList;
import java.util.List;

public class EnemyPathSystem implements BaseSystem, BaseUpdateInterface {
    private ArrayList<Entity> entities = new ArrayList<>();


    @Override
    public List<String> neededComponents() {
        List<String> output = new ArrayList<>();
        output.add(CoordsComponent.class.getName());
        output.add(PathComponent.class.getName());
        output.add(MovingComponent.class.getName());
        return output;
    }

    @Override
    public void uploadEntities(ArrayList<Entity> entities) {
        this.entities = entities;
    }

    @Override
    public void addEntity(Entity entity) {
        if(entity.getComponent(CoordsComponent.class.getName()) != null &&
                entity.getComponent(PathComponent.class.getName()) != null &&
                entity.getComponent(MovingComponent.class.getName()) != null) entities.add(entity);
    }

    @Override
    public void removeEntity(Entity entity) {
        entities.remove(entity);
    }

    @Override
    public void update(long dT) {
        CoordsComponent coordsComponent;
        MovingComponent movingComponent;
        PathComponent pathComponent;
        for(Entity entity: entities) {
            coordsComponent = (CoordsComponent) entity.getComponent(CoordsComponent.class.getName());
            movingComponent = (MovingComponent) entity.getComponent(MovingComponent.class.getName());
            pathComponent = (PathComponent) entity.getComponent(PathComponent.class.getName());

            if(pathComponent.toSecond) {
                float vecX = pathComponent.secondPointX - coordsComponent.posX;
                float vecY = pathComponent.secondPointY - coordsComponent.posY;
                double length = Math.sqrt(Math.pow(vecX, 2) + Math.pow(vecY, 2));
                movingComponent.setVector((float) (vecX / length), (float) (vecY / length));
                 if(length < movingComponent.speed) pathComponent.toSecond = !pathComponent.toSecond;
            } else {
                float vecX = pathComponent.firstPointX - coordsComponent.posX;
                float vecY = pathComponent.firstPointY - coordsComponent.posY;
                double length = Math.sqrt(Math.pow(vecX, 2) + Math.pow(vecY, 2));
                movingComponent.setVector((float) (vecX / length), (float) (vecY / length));
                if(length < movingComponent.speed) pathComponent.toSecond = !pathComponent.toSecond;
            }
        }
    }
}
