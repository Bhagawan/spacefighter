package com.example.spaceflighter.game;

import android.os.SystemClock;
import android.util.Log;

import com.example.spaceflighter.game.systems.BackgroundSystem;
import com.example.spaceflighter.game.systems.CollisionSystem;
import com.example.spaceflighter.game.systems.DrawSystem;
import com.example.spaceflighter.game.systems.EnemyPathSystem;
import com.example.spaceflighter.game.systems.EnemyShootingSystem;
import com.example.spaceflighter.game.systems.ExplodionSystem;
import com.example.spaceflighter.game.systems.InterfaceSystem;
import com.example.spaceflighter.game.systems.MovementSystem;
import com.example.spaceflighter.game.systems.OutOfFieldSystem;
import com.example.spaceflighter.game.systems.PlayerCoordsUpdateSystem;

public class Game {
    public static final byte SHOOT_SOUND = 0;
    public static final byte EXPLOSION_SOUND = 1;
    public static final double PERFECT_DT = 16.67 * 2;
    private float mWidth, mHeight; //from center of the screen
    private float mTranslationX, mTranslationY;
    private boolean gameInitialized = false;
    private GameCallback callback;

    private long survived = 0;
    private boolean pause = false;

    private Engine engine = new Engine();
    private Spawner spawner;
    private PlayerCoordsUpdateSystem playerUpdateSystem = new PlayerCoordsUpdateSystem();
    private InterfaceSystem interfaceSystem;


    public Game() { }

    public void initialize(float screenWidth, float screenHeight, float gameWidth, float gameHeight) {
        this.mWidth = gameWidth;
        this.mHeight = gameHeight;
        mTranslationX = screenWidth / (mWidth * 2);
        mTranslationY = screenHeight / (mHeight* 2);
        gameInitialized = true;

        interfaceSystem = new InterfaceSystem(gameWidth, gameHeight);
        interfaceSystem.setLifeCallback(() -> {
            pauseOn();
            if(callback != null) callback.endGame(survived);
        });
        spawner = new Spawner(engine, gameWidth, gameHeight);
        spawner.setCallback(sound -> callback.playSound(sound));

        engine.addSystem(new OutOfFieldSystem(gameWidth, gameHeight));
        engine.addSystem(new MovementSystem());
        engine.addSystem(playerUpdateSystem);
        engine.addSystem(new CollisionSystem());
        engine.addSystem(new EnemyPathSystem());
        engine.addSystem(new EnemyShootingSystem((x, y, damage) -> spawner.spawnEnemyBullet(x, y, damage)));

        engine.addSystem(new BackgroundSystem(gameWidth, gameHeight));
        engine.addSystem(new DrawSystem());
        engine.addSystem(interfaceSystem);
        ExplodionSystem explodionSystem = new ExplodionSystem();
        explodionSystem.setCallback(sound -> callback.playSound(sound));
        engine.addSystem(explodionSystem);
    }

    public void updateEngine(float[] mvpMatrix, long dT) {
        Log.d("Renderer ", String.valueOf(SystemClock.uptimeMillis()));
        if(!pause) {
            spawner.update(dT);
            engine.update(dT, mvpMatrix);
            survived += PERFECT_DT;
        }
    }

    public void onTouch(float x, float y) {
        float gameX = x / mTranslationX - mWidth;
        float gameY = mHeight - y / mTranslationY;
        if(gameX > -mWidth && gameX < mWidth && gameY > -mHeight && gameY < mHeight) playerUpdateSystem.setNewCoords(gameX, gameY);
    }

    public void newGame() {
        engine.clearEntities();
        spawner.newGame();
        survived = 0;
        playerUpdateSystem.clear();
        pauseOff();
    }

    public void setCallback(GameCallback callback) {this.callback = callback;}

    public boolean isGameInitialized() {return gameInitialized;}

    public interface GameCallback {
        void endGame(long timeSurvived);
        void playSound(byte sound);
    }

    public void pauseOn() { pause = true; }

    public void pauseOff() { pause = false; }

}
