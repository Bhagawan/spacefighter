package com.example.spaceflighter.game.systems;

import com.example.spaceflighter.assets.Textures;
import com.example.spaceflighter.game.Entity;
import com.example.spaceflighter.game.Game;
import com.example.spaceflighter.game.components.CoordsComponent;
import com.example.spaceflighter.game.components.DrawableComponent;
import com.example.spaceflighter.game.components.ExplosionComponent;
import com.example.spaceflighter.graphics.FlyingQuad;
import com.example.spaceflighter.util.SoundCallback;

import java.util.ArrayList;
import java.util.List;

public class ExplodionSystem implements BaseSystem, RenderUpdateInterface {
    private ArrayList<Entity> entities = new ArrayList<>();
    private FlyingQuad explosionQuad;
    private SoundCallback callback;

    public ExplodionSystem() {
        explosionQuad = new FlyingQuad(32, 32, Textures.EXPLOSION_SHEET);
        setStage(0);
    }

    @Override
    public List<String> neededComponents() {
        List<String> output = new ArrayList<>();
        output.add(ExplodionSystem.class.getName());
        output.add(CoordsComponent.class.getName());
        return output;
    }

    @Override
    public void uploadEntities(ArrayList<Entity> entities) {
        this.entities = entities;
    }

    @Override
    public void addEntity(Entity entity) {
        if(entity.getComponent(CoordsComponent.class.getName()) != null) {
            entities.add(entity);
            if(callback != null) callback.playSound(Game.EXPLOSION_SOUND);
            entity.addComponent(new ExplosionComponent());
        }
    }

    @Override
    public void removeEntity(Entity entity) {
        entities.remove(entity);
    }

    @Override
    public void draw(float[] mvpMatrix) {
        ExplosionComponent explosionComponent;
        CoordsComponent coordsComponent;
        DrawableComponent drawableComponent;
        for(int i = 0; i < entities.size(); i++) {
            explosionComponent = (ExplosionComponent) entities.get(i).getComponent(ExplosionComponent.class.getName());
            coordsComponent = (CoordsComponent) entities.get(i).getComponent(CoordsComponent.class.getName());
            drawableComponent = (DrawableComponent) entities.get(i).getComponent(DrawableComponent.class.getName());
            if(drawableComponent != null && explosionComponent.stage < 16 ) drawableComponent.body.draw(mvpMatrix);
            setStage(explosionComponent.stage);
            explosionQuad.setPosition(coordsComponent.posX, coordsComponent.posY);
            explosionQuad.draw(mvpMatrix);
            explosionComponent.stage++;
            if(explosionComponent.stage >= 32) {
                entities.remove(i);
                i--;
            }
        }
    }

    public void setCallback(SoundCallback callback) {
        this.callback = callback;
    }

    private void setStage(int stage) {
        float leftBorder = 0.125f * (int)(stage % 8);
        float topBorder = 0.25f * (int)(stage / 8);
        float rightBorder = 0.125f + 0.125f * (int)(stage % 8);
        float bottomBorder =  0.25f + 0.25f * (int)(stage / 8);
        explosionQuad.setTexWindow(leftBorder, topBorder, rightBorder, bottomBorder);
    }
}
