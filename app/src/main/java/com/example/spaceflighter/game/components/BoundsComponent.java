package com.example.spaceflighter.game.components;


import android.graphics.RectF;

public class BoundsComponent {
    public RectF bounds = new RectF();

    public void moveToDelta(float dX, float dY) {
        if(bounds != null) {
            bounds.top += dY;
            bounds.bottom += dY;
            bounds.left += dX;
            bounds.right += dX;
        }
    }

    public void moveTo(float newX, float newY) {
        if(bounds != null) {
            float oldTop = bounds.top;
            float oldLeft = bounds.left;
            bounds.top += newY - oldTop;
            bounds.bottom += newY - oldTop;
            bounds.left += newX - oldLeft;
            bounds.right += newX - oldLeft;
        }
    }
}
