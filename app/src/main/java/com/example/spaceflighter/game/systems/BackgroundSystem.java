package com.example.spaceflighter.game.systems;

import com.example.spaceflighter.assets.Textures;
import com.example.spaceflighter.game.Entity;
import com.example.spaceflighter.game.Game;
import com.example.spaceflighter.graphics.FlyingQuad;

import java.util.ArrayList;
import java.util.List;

public class BackgroundSystem implements BaseSystem,BaseUpdateInterface, RenderUpdateInterface {
    private FlyingQuad background;
    private float fieldWidth, fieldHeight;
    private float currOffset = 0;

    public BackgroundSystem(float fieldWidth, float fieldHeight) {
        this.fieldHeight = fieldHeight;
        this.fieldWidth = fieldWidth;
        background = new FlyingQuad(fieldWidth * 2, fieldHeight * 2, Textures.BACKGROUND_STAR);
    }

    @Override
    public List<String> neededComponents() {
        return new ArrayList<>();
    }

    @Override
    public void uploadEntities(ArrayList<Entity> entities) {

    }

    @Override
    public void addEntity(Entity entity) {

    }

    @Override
    public void removeEntity(Entity entity) {

    }

    @Override
    public void update(long dT) {
        currOffset += 0.001 * (dT / Game.PERFECT_DT);
        if(currOffset >= 1) currOffset = 0;
        float leftBorder = currOffset;
        float rightBorder =1 + currOffset;
        background.setTexWindow(leftBorder, 1, rightBorder, 0);
    }

    @Override
    public void draw(float[] mvpMatrix) {
        background.draw(mvpMatrix);
    }
}
