package com.example.spaceflighter.fragments.mvp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;

import com.example.spaceflighter.assets.Textures;
import com.example.spaceflighter.data.DatabaseHelper;
import com.example.spaceflighter.util.MyServerClient;
import com.example.spaceflighter.util.ServerClient;

import moxy.InjectViewState;
import moxy.MvpPresenter;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class GamePresenter extends MvpPresenter<GamePresenterViewInterface> {
    private final Bitmap[] assets = new Bitmap[7];

    @Override
    protected void onFirstViewAttach() {
        getViewState().showLoadingScreen();
        if(!Textures.isTexturesDownloaded()) downloadAssets();
        else getViewState().runGame();
    }

    public void endGame(Context context, long time) {
        DatabaseHelper dh = new DatabaseHelper(context);
        dh.addRecord(time);
        getViewState().runEndScreen(time);
    }

    private void downloadAssets() {
        ServerClient client = MyServerClient.createService(ServerClient.class);
        Call<ResponseBody> call = client.getBitmap("spaceship.png");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        assets[0] = BitmapFactory.decodeStream(response.body().byteStream());
                        checkLoadingProgress();
                    } else {
                        getViewState().serverError();
                    }
                } else {
                    getViewState().serverError();
                }
            }
            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                getViewState().serverError();
            }
        });
        Call<ResponseBody> call2 = client.getBitmap("spaceship_2.png");
        call2.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        assets[1] = BitmapFactory.decodeStream(response.body().byteStream());
                        checkLoadingProgress();
                    } else {
                        getViewState().serverError();
                    }
                } else {
                    getViewState().serverError();
                }
            }
            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                getViewState().serverError();
            }
        });
        Call<ResponseBody> call3 = client.getBitmap("spaceship_3.png");
        call3.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        assets[2] = BitmapFactory.decodeStream(response.body().byteStream());
                        checkLoadingProgress();
                    } else {
                        getViewState().serverError();
                    }
                } else {
                    getViewState().serverError();
                }
            }
            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                getViewState().serverError();
            }
        });
        Call<ResponseBody> call4 = client.getBitmap("spaceship_4.png");
        call4.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        assets[3] = BitmapFactory.decodeStream(response.body().byteStream());
                        checkLoadingProgress();
                    } else {
                        getViewState().serverError();
                    }
                } else {
                    getViewState().serverError();
                }
            }
            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                getViewState().serverError();
            }
        });
        Call<ResponseBody> call5 = client.getBitmap("shot.png");
        call5.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        assets[4] = BitmapFactory.decodeStream(response.body().byteStream());
                        checkLoadingProgress();
                    } else {
                        getViewState().serverError();
                    }
                } else {
                    getViewState().serverError();
                }
            }
            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                getViewState().serverError();
            }
        });
        Call<ResponseBody> call6 = client.getBitmap("star_back.png");
        call6.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        assets[5] = BitmapFactory.decodeStream(response.body().byteStream());
                        checkLoadingProgress();
                    } else {
                        getViewState().serverError();
                    }
                } else {
                    getViewState().serverError();
                }
            }
            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                getViewState().serverError();
            }
        });
        Call<ResponseBody> call7 = client.getBitmap("explosion.png");
        call7.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        assets[6] = BitmapFactory.decodeStream(response.body().byteStream());
                        checkLoadingProgress();
                    } else {
                        getViewState().serverError();
                    }
                } else {
                    getViewState().serverError();
                }
            }
            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                getViewState().serverError();
            }
        });
    }

    private void checkLoadingProgress() {
        boolean allLoaded = true;
        for (Bitmap asset : assets)
            if (asset == null) {
                allLoaded = false;
                break;
            }
        if(allLoaded) {
            Textures.setBitmaps(assets);
            getViewState().runGame();
        }
    }
}
