package com.example.spaceflighter.game.components;

public class EnemyShootingComponent {
    public long shootInterval = 5000; // milliseconds
    public long currentShootInterval = 0;
    public int damage;

    public EnemyShootingComponent(int damage) {
        this.damage = damage;
    }

    public EnemyShootingComponent(long shootInterval, int damage) {
        this.shootInterval = shootInterval;
        this.damage = damage;
    }
}
