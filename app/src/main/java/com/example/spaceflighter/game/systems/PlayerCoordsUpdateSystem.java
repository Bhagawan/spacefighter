package com.example.spaceflighter.game.systems;

import com.example.spaceflighter.game.Entity;
import com.example.spaceflighter.game.components.BoundsComponent;
import com.example.spaceflighter.game.components.CoordsComponent;
import com.example.spaceflighter.game.components.DrawableComponent;
import com.example.spaceflighter.game.components.PlayerComponent;

import java.util.ArrayList;
import java.util.List;

public class PlayerCoordsUpdateSystem implements BaseSystem, BaseUpdateInterface {
    private Entity player;
    private float newX, newY;
    private boolean newCoords = false;

    @Override
    public void update(long dT) {
        if(player != null && newCoords) {
            Object playerCoords = player.getComponent(CoordsComponent.class.getName());
            Object drawable = player.getComponent(DrawableComponent.class.getName());
            Object boundsComponent = player.getComponent(BoundsComponent.class.getName());
            if(playerCoords != null && drawable != null) {
                ((CoordsComponent) playerCoords).posX = newX;
                ((CoordsComponent) playerCoords).posY = newY;
                ((DrawableComponent) drawable).body.setPosition(newX, newY);
                if(boundsComponent != null) ((BoundsComponent)boundsComponent).moveTo(newX, newY);
                newCoords = false;
            }
        }
    }

    @Override
    public List<String> neededComponents() {
        List<String> output = new ArrayList<>();
        output.add(PlayerComponent.class.getName());
        return output;
    }

    @Override
    public void uploadEntities(ArrayList<Entity> entities) {
        for(Entity entity: entities) if(entity.getComponent(PlayerComponent.class.getName()) != null) player = entity;
    }

    @Override
    public void addEntity(Entity entity) {
        if(entity.getComponent(PlayerComponent.class.getName()) != null) player = entity;
    }

    @Override
    public void removeEntity(Entity entity) {

    }

    public void setNewCoords(float newX, float newY) {
        this.newX = newX;
        this.newY = newY;
        newCoords = true;
    }

    public void clear() {
        newCoords = false;
    }
}
