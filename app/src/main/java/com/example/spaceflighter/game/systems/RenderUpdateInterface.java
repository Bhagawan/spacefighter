package com.example.spaceflighter.game.systems;

public interface RenderUpdateInterface {
    void draw(float[] mvpMatrix);
}
