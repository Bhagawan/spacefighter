package com.example.spaceflighter.game.systems;

import com.example.spaceflighter.assets.Textures;
import com.example.spaceflighter.game.Entity;
import com.example.spaceflighter.game.components.HealthComponent;
import com.example.spaceflighter.game.components.PlayerComponent;
import com.example.spaceflighter.graphics.Quad;

import java.util.ArrayList;
import java.util.List;

public class InterfaceSystem implements BaseSystem, RenderUpdateInterface {
    private HealthComponent playerHealth;
    private Quad heart_icon;
    private ArrayList<Quad> lifeNumber = new ArrayList<>();
    private float gameWidth, gameHeight;
    private LifeCallback lifeCallback;

    public InterfaceSystem(float gameWidth, float gameHeight) {
        this.gameWidth = gameWidth;
        this.gameHeight = gameHeight;
        heart_icon = new Quad(10, 10, Textures.HEART);
        heart_icon.setPosition( 11 - gameWidth, 11 - gameHeight);
    }

    @Override
    public void draw(float[] mvpMatrix) {
        if(playerHealth != null) updateLife();
        heart_icon.draw(mvpMatrix);
        for(Quad quad: lifeNumber) quad.draw(mvpMatrix);
    }

    @Override
    public List<String> neededComponents() {
        List<String> output = new ArrayList<>();
        output.add(PlayerComponent.class.getName());
        output.add(HealthComponent.class.getName());
        return output;
    }

    @Override
    public void uploadEntities(ArrayList<Entity> entities) {
        for(Entity entity: entities) {
            HealthComponent healthComponent = (HealthComponent) entity.getComponent(HealthComponent.class.getName());
            PlayerComponent playerComponent = (PlayerComponent) entity.getComponent(PlayerComponent.class.getName());
            if(healthComponent != null && playerComponent != null) playerHealth = healthComponent;
        }
    }

    @Override
    public void addEntity(Entity entity) {
        HealthComponent healthComponent = (HealthComponent) entity.getComponent(HealthComponent.class.getName());
        PlayerComponent playerComponent = (PlayerComponent) entity.getComponent(PlayerComponent.class.getName());
        if(healthComponent != null && playerComponent != null) playerHealth = healthComponent;
    }

    @Override
    public void removeEntity(Entity entity) {
        HealthComponent healthComponent = (HealthComponent) entity.getComponent(HealthComponent.class.getName());
        PlayerComponent playerComponent = (PlayerComponent) entity.getComponent(PlayerComponent.class.getName());
        if(healthComponent != null && playerComponent != null) playerHealth = null;
    }

    private void updateLife() {
        int life = playerHealth.health;
        byte n = 0;
        if(life <= 0 && lifeCallback != null) {
            lifeCallback.dead();
            n = 1;
        }
        while(life > 0) {
            n++;
            if(lifeNumber.size() < n) {
                Quad newNumber = new Quad(8, 10, Textures.numbers[0]);
                newNumber.setPosition(heart_icon.getWidth() + 12 + (4.5f * lifeNumber.size()) - gameWidth, 11 - gameHeight);
                lifeNumber.add(newNumber);
            }
            life = life / 10;
        }
        while(lifeNumber.size() > n) lifeNumber.remove(n);
        life = playerHealth.health;
        if(life <= 0 ) {
            lifeNumber.get(0).setTexture(Textures.numbers[0]);
        } else {
            while(n > 0) {
                int last = life % 10;
                lifeNumber.get(n-1).setTexture(Textures.numbers[last]);
                life = life / 10;
                n--;
            }
        }
    }

    public void setLifeCallback(LifeCallback lifeCallback) {
        this.lifeCallback = lifeCallback;
    }

    public interface LifeCallback {
        void dead();
    }
}
