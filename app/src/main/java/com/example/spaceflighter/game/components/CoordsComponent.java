package com.example.spaceflighter.game.components;

public class CoordsComponent {
    public float posX, posY;

    public CoordsComponent(float posX, float posY) {
        this.posX = posX;
        this.posY = posY;
    }
}
