package com.example.spaceflighter.util;

public interface SoundCallback {
    void playSound(byte sound);
}
