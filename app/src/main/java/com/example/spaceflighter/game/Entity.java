package com.example.spaceflighter.game;

import java.util.ArrayList;

public class Entity {
    private boolean dead = false;

    private String id = "";
    private ArrayList<Object> components = new ArrayList<>();

    public ArrayList<Object> getComponents() {
        return components;
    }

    public void addComponent(Object component) {
        components.add(component);
    }

    public Object getComponent(String className) {
        for(Object component : components) {
            if(component.getClass().getName().equals(className)) return component;
        }
        return null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isDead() {
        return dead;
    }

    public void kill() {
        dead = true;
    }
}
