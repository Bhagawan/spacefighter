package com.example.spaceflighter.game.components;

public class PathComponent {
    public float firstPointX, firstPointY, secondPointX, secondPointY;
    public boolean toSecond = true;

    public PathComponent(float firstPointX, float firstPointY, float secondPointX, float secondPointY) {
        this.firstPointX = firstPointX;
        this.firstPointY = firstPointY;
        this.secondPointX = secondPointX;
        this.secondPointY = secondPointY;
    }
}
