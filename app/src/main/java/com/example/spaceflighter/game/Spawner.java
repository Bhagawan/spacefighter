package com.example.spaceflighter.game;

import android.graphics.RectF;

import com.example.spaceflighter.assets.Textures;
import com.example.spaceflighter.game.components.BonusComponent;
import com.example.spaceflighter.game.components.BoundsComponent;
import com.example.spaceflighter.game.components.CoordsComponent;
import com.example.spaceflighter.game.components.DamageComponent;
import com.example.spaceflighter.game.components.DrawableComponent;
import com.example.spaceflighter.game.components.EnemyShootingComponent;
import com.example.spaceflighter.game.components.HealthComponent;
import com.example.spaceflighter.game.components.MovingComponent;
import com.example.spaceflighter.game.components.PathComponent;
import com.example.spaceflighter.game.components.PlayerComponent;
import com.example.spaceflighter.graphics.Quad;
import com.example.spaceflighter.util.SoundCallback;

import java.util.Random;

public class Spawner {
    public static final float SHIP_WIDTH = 20.0f;
    public static final float SHIP_HEIGHT = 20.0f;
    private Engine engine;
    private float gameWidth, gameHeight;

    private Entity spaceShip;
    private PlayerComponent playerStats;
    private long shootInterval = 0;
    private long objectSpawnInterval = 3000;
    private long currentObjectSpawnInterval = 0;
    private final long bonusSpawnInterval = 10000;
    private long currentBonusSpawnInterval = 0;
    private int difficulty = 0;

    private SoundCallback soundCallback;

    public Spawner(Engine engine, float gameWidth, float gameHeight) {
        this.engine = engine;
        this.gameWidth = gameWidth;
        this.gameHeight = gameHeight;
        spawnPlayer();
    }

    public void update(long dT) {
        shootInterval += dT;
        currentObjectSpawnInterval += dT;
        currentBonusSpawnInterval += dT;
        if(shootInterval >= playerStats.fireRate) {
            shootInterval = 0;
            fire();
        }
        if(currentObjectSpawnInterval >= objectSpawnInterval) {
            currentObjectSpawnInterval = 0;
            spawnObject();
            objectSpawnInterval--;
        }
        if(currentBonusSpawnInterval >= bonusSpawnInterval) {
            currentBonusSpawnInterval = 0;
            spawnBonus();
            difficulty+=15;
        }
    }

    private void spawnObject() {
        int enemySize = 25;
        Entity object = new Entity();

        DrawableComponent drawableComponent = new DrawableComponent();
        object.addComponent(drawableComponent);
        HealthComponent healthComponent = new HealthComponent();
        DamageComponent damageComponent = new DamageComponent();
        object.addComponent(healthComponent);
        object.addComponent(damageComponent);

        MovingComponent movingComponent = new MovingComponent(0.0f, -1.0f, 1);
        object.addComponent(movingComponent);

        int type = new Random().nextInt(100 + difficulty);
        if(type < 10 + difficulty) {
            drawableComponent.body = new Quad(enemySize,enemySize, Textures.ENEMY_2);
            healthComponent.health = 300 + difficulty * 2;
            damageComponent.damage = 50 + difficulty;
            movingComponent.setSpeed(0.3f);
            PathComponent pathComponent = new PathComponent((enemySize * 0.5f) - gameWidth, gameHeight - (enemySize * 0.5f),
                    gameWidth - (enemySize * 0.5f), gameHeight - (enemySize * 0.5f));
            object.addComponent(pathComponent);
            object.addComponent(new EnemyShootingComponent(50));
        } else if( type < (10 + difficulty) + (90 - difficulty / 2)) {
            drawableComponent.body = new Quad(enemySize,enemySize, Textures.ENEMY_3);
            healthComponent.health = 50 + difficulty * 2;
            damageComponent.damage = 10 + difficulty;
            movingComponent.setSpeed(1.3f);
        } else {
            drawableComponent.body = new Quad(enemySize,enemySize, Textures.ENEMY_1);
            healthComponent.health = 100 + difficulty * 2;
            damageComponent.damage = 10 + difficulty;
            movingComponent.setSpeed(0.7f);
        }

        Random r = new Random();
        float randX = (enemySize * 0.5f) - gameWidth + r.nextFloat() * (2 * gameWidth - (enemySize * 0.5f));
        CoordsComponent coordsComponent = new CoordsComponent(randX, gameHeight - enemySize * 0.5f);
        object.addComponent(coordsComponent);
        drawableComponent.body.setPosition(coordsComponent.posX, coordsComponent.posY);

        BoundsComponent boundsComponent = new BoundsComponent();
        boundsComponent.bounds = new RectF(coordsComponent.posX - (enemySize * 0.5f), coordsComponent.posY + (enemySize * 0.5f), coordsComponent.posX + (enemySize * 0.5f), coordsComponent.posY - (enemySize * 0.3f) );
        object.addComponent(boundsComponent);

        object.setId("enemy");
        engine.addEntity(object);
    }

    private void spawnBonus() {
        int bonusSize = 15;
        Entity object = new Entity();

        DrawableComponent drawableComponent = new DrawableComponent();
        object.addComponent(drawableComponent);
        BonusComponent bonusComponent = new BonusComponent();
        object.addComponent(bonusComponent);

        switch (new Random().nextInt(4)) {
            case 0:
                drawableComponent.body = new Quad(bonusSize, bonusSize, Textures.BONUS_GUN);
                drawableComponent.body.setColor(1.0f, 0.0f, 0.0f, 1.0f);
                bonusComponent.gun_damage_up = 5;
                break;
            case 1:
                drawableComponent.body = new Quad(bonusSize, bonusSize, Textures.BONUS_GUN);
                drawableComponent.body.setColor(0.0f, 0.0f, 1.0f, 1.0f);
                bonusComponent.fireRate_up = 30;
                break;
            case 2:
                drawableComponent.body = new Quad(bonusSize, bonusSize, Textures.BONUS_GUN);
                drawableComponent.body.setColor(0.0f, 1.0f, 0.0f, 1.0f);
                bonusComponent.gun_bullets_up = 1;
                break;
            case 3:
                drawableComponent.body = new Quad(bonusSize, bonusSize, Textures.BONUS_HEALTH);
                bonusComponent.health_bonus = 10;
                break;
        }

        Random r = new Random();
        float randX = (bonusSize * 0.5f) - gameWidth + r.nextFloat() * (2 * gameWidth - bonusSize);
        CoordsComponent coordsComponent = new CoordsComponent(randX, gameHeight - bonusSize * 0.5f);
        object.addComponent(coordsComponent);
        drawableComponent.body.setPosition(coordsComponent.posX, coordsComponent.posY);

        BoundsComponent boundsComponent = new BoundsComponent();
        boundsComponent.bounds = new RectF(coordsComponent.posX - (bonusSize * 0.5f), coordsComponent.posY + (bonusSize * 0.5f), coordsComponent.posX + (bonusSize * 0.5f), coordsComponent.posY - (bonusSize * 0.5f) );
        object.addComponent(boundsComponent);

        MovingComponent movingComponent = new MovingComponent(0.0f, -1.0f, 1);
        object.addComponent(movingComponent);

        object.setId("bonus");
        engine.addEntity(object);
    }

    private void fire() {
        PlayerComponent playerComponent = (PlayerComponent) spaceShip.getComponent(PlayerComponent.class.getName());
        CoordsComponent shipCoords = (CoordsComponent) spaceShip.getComponent(CoordsComponent.class.getName());

        float dAngle = playerComponent.bulletsAmount <= 1 ? playerComponent.bulletSpreadAngle * 2 : (playerComponent.bulletSpreadAngle * 2) / (playerComponent.bulletsAmount - 1);

        float a = 90 + playerComponent.bulletSpreadAngle;
        for(int i = 0; i < playerComponent.bulletsAmount; i++) {
            Entity firstBullet = new Entity();

            DrawableComponent drawableComponent = new DrawableComponent();
            drawableComponent.body = new Quad(5.0f, 5.0f, Textures.SHOT_BLUE);
            drawableComponent.body.setColor(200.0f, 229.0f, 0.0f, 1.0f);
            drawableComponent.body.setAngle(a - 90);
            firstBullet.addComponent(drawableComponent);

            CoordsComponent coordsComponent = new CoordsComponent(shipCoords.posX - SHIP_WIDTH / 3, shipCoords.posY + SHIP_HEIGHT / 4);
            firstBullet.addComponent(coordsComponent);
            drawableComponent.body.setPosition(coordsComponent.posX, coordsComponent.posY);

            MovingComponent movingComponent = new MovingComponent((float) Math.cos((a * Math.PI)/ 180), (float) Math.sin((a * Math.PI)/ 180), 1);
            firstBullet.addComponent(movingComponent);

            BoundsComponent boundsComponent = new BoundsComponent();
            boundsComponent.bounds = new RectF(coordsComponent.posX - 2.5f, coordsComponent.posY + 2.5f, coordsComponent.posX + 2.5f, coordsComponent.posY - 2.5f);
            firstBullet.addComponent(boundsComponent);

            HealthComponent healthComponent = new HealthComponent(1);
            DamageComponent damageComponent = new DamageComponent(playerComponent.bulletDamage);
            firstBullet.addComponent(healthComponent);
            firstBullet.addComponent(damageComponent);

            firstBullet.setId("bullet");
            engine.addEntity(firstBullet);

            Entity secondBullet = new Entity();

            DrawableComponent secondDrawableComponent = new DrawableComponent();
            secondDrawableComponent.body = new Quad(5.0f, 5.0f, Textures.SHOT_BLUE);
            secondDrawableComponent.body.setColor(200.0f, 229.0f, 0.0f, 1.0f);
            secondDrawableComponent.body.setAngle(a - 90);
            secondBullet.addComponent(secondDrawableComponent);

            CoordsComponent secondCoordsComponent = new CoordsComponent(shipCoords.posX + SHIP_WIDTH / 3, shipCoords.posY + SHIP_HEIGHT / 4);
            secondBullet.addComponent(secondCoordsComponent);
            secondDrawableComponent.body.setPosition(secondCoordsComponent.posX, secondCoordsComponent.posY);

            MovingComponent secondMovingComponent = new MovingComponent((float) Math.cos((a * Math.PI)/ 180), (float) Math.sin((a * Math.PI)/ 180), 1);
            secondBullet.addComponent(secondMovingComponent);

            BoundsComponent secondBoundsComponent = new BoundsComponent();
            secondBoundsComponent.bounds = new RectF(secondCoordsComponent.posX - 2.5f, secondCoordsComponent.posY + 2.5f, secondCoordsComponent.posX + 2.5f, secondCoordsComponent.posY - 2.5f);
            secondBullet.addComponent(secondBoundsComponent);

            HealthComponent secondHealthComponent = new HealthComponent(1);
            DamageComponent secondDamageComponent = new DamageComponent(playerComponent.bulletDamage);
            secondBullet.addComponent(secondHealthComponent);
            secondBullet.addComponent(secondDamageComponent);

            secondBullet.setId("bullet");
            engine.addEntity(secondBullet);
            a -= dAngle;
        }
        if(soundCallback != null) soundCallback.playSound(Game.SHOOT_SOUND);
    }

    public void spawnEnemyBullet(float x, float y, int damage) {
        Entity secondBullet = new Entity();

        DrawableComponent secondDrawableComponent = new DrawableComponent();
        secondDrawableComponent.body = new Quad(10.0f, 10.0f, Textures.SHOT_BLUE);
        secondDrawableComponent.body.setColor(1.0f, 0.0f, 0.0f, 1.0f);
        secondDrawableComponent.body.setAngle(180);
        secondBullet.addComponent(secondDrawableComponent);

        CoordsComponent secondCoordsComponent = new CoordsComponent(x, y);
        secondBullet.addComponent(secondCoordsComponent);
        secondDrawableComponent.body.setPosition(secondCoordsComponent.posX, secondCoordsComponent.posY);

        MovingComponent secondMovingComponent = new MovingComponent(0.0f, -1.0f, 1);
        secondBullet.addComponent(secondMovingComponent);

        BoundsComponent secondBoundsComponent = new BoundsComponent();
        secondBoundsComponent.bounds = new RectF(secondCoordsComponent.posX - 5.0f, secondCoordsComponent.posY + 5.0f, secondCoordsComponent.posX + 5.0f, secondCoordsComponent.posY - 5.0f);
        secondBullet.addComponent(secondBoundsComponent);

        HealthComponent secondHealthComponent = new HealthComponent(1);
        DamageComponent secondDamageComponent = new DamageComponent(damage);
        secondBullet.addComponent(secondHealthComponent);
        secondBullet.addComponent(secondDamageComponent);

        secondBullet.setId("enemy_bullet");
        engine.addEntity(secondBullet);
    }

    public void newGame() {
        spawnPlayer();
        difficulty = 0;
        currentBonusSpawnInterval = 0;
        currentObjectSpawnInterval = 0;
        shootInterval = 0;
    }

    public void setCallback(SoundCallback callback) { this.soundCallback = callback;}

    private void spawnPlayer() {
        spaceShip = new Entity();
        spaceShip.setId("player");
        DrawableComponent drawableComponent = new DrawableComponent();
        drawableComponent.body = new Quad(SHIP_WIDTH, SHIP_HEIGHT, Textures.SPACESHIP);
        spaceShip.addComponent(drawableComponent);

        CoordsComponent coordsComponent = new CoordsComponent(0, SHIP_HEIGHT - gameHeight);
        spaceShip.addComponent(coordsComponent);
        drawableComponent.body.setPosition(coordsComponent.posX, coordsComponent.posY);

        BoundsComponent boundsComponent = new BoundsComponent();
        boundsComponent.bounds = new RectF(coordsComponent.posX - SHIP_WIDTH * 0.4f, coordsComponent.posY + SHIP_HEIGHT * 0.4f, coordsComponent.posX + SHIP_WIDTH * 0.4f, coordsComponent.posY - SHIP_HEIGHT * 0.5f);
        spaceShip.addComponent(boundsComponent);

        HealthComponent healthComponent = new HealthComponent(100);
        DamageComponent damageComponent = new DamageComponent(999);
        spaceShip.addComponent(healthComponent);
        spaceShip.addComponent(damageComponent);

        playerStats = new PlayerComponent(1, 0, 30);
        spaceShip.addComponent(playerStats);

        engine.addEntity(spaceShip);
    }

}
