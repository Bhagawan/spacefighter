package com.example.spaceflighter.fragments.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.spaceflighter.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RecordsAdapter extends RecyclerView.Adapter<RecordsAdapter.ViewHolder> {

    private List<Long> records;

    public RecordsAdapter(List<Long> data) {
        this.records = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler_records, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.place.setText(String.valueOf(position + 1));
        DateFormat formatter = new SimpleDateFormat("mm:ss", Locale.getDefault());
        String timeString = formatter.format(new Date(records.get(position)));
        holder.time.setText(timeString);
    }

    @Override
    public int getItemCount() {
        if(records == null) return 0;
        else return records.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView time;
        private final TextView place;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            place = itemView.findViewById(R.id.text_record_item_number);
            time = itemView.findViewById(R.id.text_record_item_time);
        }
    }
}
