package com.example.spaceflighter.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

public class Util {

    public static Bitmap svgToBitmap(Drawable vectorDrawable, int x, int y) {
        Bitmap bitmap = Bitmap.createBitmap(x,
                y, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    public static Bitmap createTxtImage(String string, int txtSize, int color) {
        Bitmap bitmap = Bitmap.createBitmap(string.length() * txtSize + 4,
                txtSize + 4, Bitmap.Config.ARGB_8888);
        Canvas canvasTemp = new Canvas(bitmap);
        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setColor(color);
        p.setTextSize(txtSize);
        canvasTemp.drawText(string, 2, txtSize - 2, p);
        return bitmap;
    }

    public static Bitmap createColorBitmap(int width, int height, int color) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(color);
        return bitmap;
    }

    public static Bitmap createFieldBitmap(int squareSize, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width * squareSize, height * squareSize, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint p = new Paint();
        for(int y = 0; y < height; y++) {
            for(int x = 0; x < width; x++) {
                if((x + y) % 2 == 0) p.setColor(Color.parseColor("#389C3C"));
                else p.setColor(Color.parseColor("#236726"));
                canvas.drawRect(x * squareSize, y * squareSize, (x + 1) * squareSize, (y + 1) * squareSize, p);
            }
        }
        return bitmap;
    }

    public static Bitmap createEyeBitmap() {
        Bitmap bitmap = Bitmap.createBitmap(32, 32, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint p = new Paint();
        p.setColor(Color.WHITE);
        canvas.drawOval(0, 0, 32,32, p);
        p.setColor(Color.BLACK);
        canvas.drawOval(16, 8, 0,24, p);
        return bitmap;
    }
}
