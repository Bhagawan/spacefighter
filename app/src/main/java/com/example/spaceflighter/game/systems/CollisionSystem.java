package com.example.spaceflighter.game.systems;

import android.graphics.RectF;

import com.example.spaceflighter.game.Entity;
import com.example.spaceflighter.game.components.BonusComponent;
import com.example.spaceflighter.game.components.BoundsComponent;
import com.example.spaceflighter.game.components.DamageComponent;
import com.example.spaceflighter.game.components.ExplosionComponent;
import com.example.spaceflighter.game.components.HealthComponent;
import com.example.spaceflighter.game.components.PlayerComponent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class CollisionSystem implements BaseSystem, BaseUpdateInterface  {
    private final int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    private ArrayList<Entity> entities = new ArrayList<>();
    private ThreadPoolExecutor executor = new ThreadPoolExecutor(NUMBER_OF_CORES * 2, NUMBER_OF_CORES * 2,
            0, TimeUnit.SECONDS, new LinkedBlockingQueue<>());

    @Override
    public void update(long dT) {
        if(entities.size() > 1) {
            for(int i = 0; i < entities.size() - 1; i++) {
                for(int j = i + 1; j < entities.size(); j++) {
                    final int[] n = new int[2];
                    n[0] = i;
                    n[1] = j;
                    executor.execute(() -> collide(this.entities.get(n[0]), this.entities.get(n[1])));
                }
            }
            synchronized (this) {
                while(executor.getActiveCount() > 0) {
                    try {
                        wait(1);
                    } catch (InterruptedException ignored) {

                    }
                }
            }
        }
    }

    @Override
    public List<String> neededComponents() {
        List<String> output = new ArrayList<>();
        output.add(BoundsComponent.class.getName());
        return output;
    }

    @Override
    public void uploadEntities(ArrayList<Entity> entities) {
        this.entities = entities;
    }

    @Override
    public void addEntity(Entity entity) {
        if(entity.getComponent(BoundsComponent.class.getName()) != null) entities.add(entity);
    }

    @Override
    public void removeEntity(Entity entity) {
        entities.remove(entity);
    }

    public static boolean intersects(RectF a,RectF b) {
        return a.left < b.right && b.left < a.right
                && a.top > b.bottom && b.top > a.bottom;
    }

    private void collide(Entity a, Entity b) {
        HealthComponent firstHealthComponent = (HealthComponent) a.getComponent(HealthComponent.class.getName());
        DamageComponent firstDamageComponent = (DamageComponent) a.getComponent(DamageComponent.class.getName());
        BoundsComponent firstBoundsComponent = (BoundsComponent) a.getComponent(BoundsComponent.class.getName());

        HealthComponent secondHealthComponent = (HealthComponent) b.getComponent(HealthComponent.class.getName());
        DamageComponent secondDamageComponent = (DamageComponent) b.getComponent(DamageComponent.class.getName());
        BoundsComponent secondBoundsComponent = (BoundsComponent) b.getComponent(BoundsComponent.class.getName());
        if(intersects(firstBoundsComponent.bounds, secondBoundsComponent.bounds)) {
            if(a.getId().equals("player") && b.getId().equals("bonus")) {
                BonusComponent bonusComponent = (BonusComponent) b.getComponent(BonusComponent.class.getName());
                PlayerComponent playerComponent = (PlayerComponent) a.getComponent(PlayerComponent.class.getName());
                if(bonusComponent != null && playerComponent != null) {
                    playerComponent.bulletDamage += bonusComponent.gun_damage_up;
                    playerComponent.bulletsAmount += bonusComponent.gun_bullets_up;
                    firstHealthComponent.health += bonusComponent.health_bonus;
                    playerComponent.bulletSpreadAngle = playerComponent.bulletsAmount * 2;
                    playerComponent.fireRate -= bonusComponent.fireRate_up;
                }
                b.kill();
            } else if(a.getId().equals("bonus") && b.getId().equals("player")) {
                BonusComponent bonusComponent = (BonusComponent) a.getComponent(BonusComponent.class.getName());
                PlayerComponent playerComponent = (PlayerComponent) b.getComponent(PlayerComponent.class.getName());
                if(bonusComponent != null && playerComponent != null) {
                    playerComponent.bulletDamage += bonusComponent.gun_damage_up;
                    playerComponent.bulletsAmount += bonusComponent.gun_bullets_up;
                    secondHealthComponent.health += bonusComponent.health_bonus;
                    playerComponent.bulletSpreadAngle = playerComponent.bulletsAmount;
                    playerComponent.fireRate -= bonusComponent.fireRate_up;
                }
                a.kill();
            } else if((a.getId().equals("bonus") && b.getId().equals("enemy")) ||
                    (a.getId().equals("enemy") && b.getId().equals("bonus")) ||
                    (a.getId().equals("enemy") && b.getId().equals("enemy")) ||
                    (a.getId().equals("enemy_bullet") && b.getId().equals("enemy")) ||
                    (a.getId().equals("enemy") && b.getId().equals("enemy_bullet")) ||
                    (a.getId().equals("enemy_bullet") && b.getId().equals("bullet")) ||
                    (a.getId().equals("bullet") && b.getId().equals("enemy_bullet")) ||
                    (a.getId().equals("player") && b.getId().equals("bullet")) ||
                    (a.getId().equals("bullet") && b.getId().equals("player")) ||
                    (a.getId().equals("bullet") && b.getId().equals("bullet"))) {

                // do nothing

            } else {
                if(firstHealthComponent != null && secondDamageComponent != null ) {
                    firstHealthComponent.health -= secondDamageComponent.damage;
                    if(firstHealthComponent.health <= 0) {
                        if(a.getId().equals("enemy")) a.addComponent(new ExplosionComponent());
                        a.kill();
                    }
                }
                if(secondHealthComponent != null && firstDamageComponent != null ) {
                    secondHealthComponent.health -= firstDamageComponent.damage;
                    if(secondHealthComponent.health <= 0) {
                        if(b.getId().equals("enemy")) b.addComponent(new ExplosionComponent());
                        b.kill();
                    }
                }
            }
        }
    }
}
