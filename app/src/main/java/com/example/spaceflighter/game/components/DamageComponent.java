package com.example.spaceflighter.game.components;

public class DamageComponent {
    public int damage;

    public DamageComponent() {}

    public DamageComponent(int damage) {
        this.damage = damage;
    }
}
