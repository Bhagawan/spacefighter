package com.example.spaceflighter.game.systems;

import com.example.spaceflighter.game.Entity;
import com.example.spaceflighter.game.components.DrawableComponent;

import java.util.ArrayList;
import java.util.List;

public class DrawSystem implements BaseSystem, RenderUpdateInterface {
    ArrayList<Entity> drawableObjects = new ArrayList<>();

    @Override
    public void draw(float[] mvpMatrix) {
        for(Entity entity: drawableObjects) {
            Object component = entity.getComponent(DrawableComponent.class.getName());
            if(component != null) {
                ((DrawableComponent) component).body.draw(mvpMatrix);
            }
        }
    }

    @Override
    public List<String> neededComponents() {
        List<String> output = new ArrayList<>();
        output.add(DrawableComponent.class.getName());
        return output;
    }

    @Override
    public void uploadEntities(ArrayList<Entity> entities) {
        drawableObjects = entities;
    }

    @Override
    public void addEntity(Entity entity) {
        if(entity.getComponent(DrawableComponent.class.getName()) != null) drawableObjects.add(entity);
    }

    @Override
    public void removeEntity(Entity entity) {
        drawableObjects.remove(entity);
    }
}
