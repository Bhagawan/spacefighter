package com.example.spaceflighter.data;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "records_db";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Record.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Record.TABLE_NAME);
        onCreate(db);
    }

    public void clearDb() {
        SQLiteDatabase db = getWritableDatabase();
        onUpgrade(db,1,1);
    }

    public long addRecord(long time) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Record.COLUMN_TIME, time);

        long id = db.insert(Record.TABLE_NAME, null, values);

        db.close();
        return id;
    }

    public Record getRecord(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Record.TABLE_NAME,
                new String[]{ Record.COLUMN_ID, Record.COLUMN_TIME, Record.COLUMN_TIMESTAMP},
                Record.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null) cursor.moveToFirst();

        @SuppressLint("Range")
        Record bet = new Record(
                cursor.getInt(cursor.getColumnIndex(Record.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(Record.COLUMN_TIMESTAMP)),
                cursor.getInt(cursor.getColumnIndex(Record.COLUMN_TIME)));

        cursor.close();
        db.close();
        return bet;
    }

    @SuppressLint("Range")
    public ArrayList<Long> getAllRecords() {
        ArrayList<Long> records = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + Record.TABLE_NAME + " ORDER BY " +
                Record.COLUMN_TIME + " DESC";

        try (SQLiteDatabase db = this.getWritableDatabase(); Cursor cursor = db.rawQuery(selectQuery, null)) {
            if (null != cursor && cursor.moveToFirst()) {
                do {
                    records.add((long) cursor.getInt(cursor.getColumnIndex(Record.COLUMN_TIME)));
                } while (cursor.moveToNext());
            }
        }

        return records;
    }

    public int getNotesCount() {
        String countQuery = "SELECT  * FROM " + Record.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    public void deleteBet(Record note) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Record.TABLE_NAME, Record.COLUMN_ID + " = ?", new String[]{String.valueOf(note.getId())});
        db.close();
    }

}
