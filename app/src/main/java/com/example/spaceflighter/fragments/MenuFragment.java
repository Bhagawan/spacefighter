package com.example.spaceflighter.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.spaceflighter.GameActivity;
import com.example.spaceflighter.R;
import com.example.spaceflighter.assets.Textures;
import com.example.spaceflighter.util.SharedPref;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class MenuFragment extends Fragment {
    private View mView;
    private Target target;
    private boolean soundsOn;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                requireActivity().finish();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_menu, container, false);
        setBackground();
        AppCompatButton newGameButton = mView.findViewById(R.id.button_menu_play);
        newGameButton.setOnClickListener(v -> startActivity(new Intent(getContext(), GameActivity.class)));
        AppCompatButton recordsButton = mView.findViewById(R.id.button_menu_records);
        recordsButton.setOnClickListener(v -> startActivity(new Intent(getContext(), RecordsActivity.class)));

        ImageView soundIndicator = mView.findViewById(R.id.imageView_menu_sound);
        soundsOn = SharedPref.getSoundSettings(getContext());
        if(!soundsOn) soundIndicator.setImageResource(R.drawable.ic_baseline_volume_off_24);
        soundIndicator.setOnClickListener(v -> {
            soundsOn = !soundsOn;
            SharedPref.setSound(getContext(), soundsOn);
            if(soundsOn) soundIndicator.setImageResource(R.drawable.ic_baseline_volume_up_24);
            else soundIndicator.setImageResource(R.drawable.ic_baseline_volume_off_24);
        });

        return mView;
    }

    private void setBackground() {
        ConstraintLayout back = mView.findViewById(R.id.layout_menu);
        if(Textures.isBackgroundLoaded()) {
            back.setBackground(new BitmapDrawable(getResources(), Textures.background));
        } else {
            target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    Textures.setBackground(bitmap);
                    back.setBackground(new BitmapDrawable(getResources(), bitmap));
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            Picasso.get().load("http://195.201.125.8/SpaceFighter/Assets/star_back.png").into(target);
        }
    }

}