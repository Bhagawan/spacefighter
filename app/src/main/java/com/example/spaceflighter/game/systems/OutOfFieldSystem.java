package com.example.spaceflighter.game.systems;

import com.example.spaceflighter.game.Entity;
import com.example.spaceflighter.game.components.CoordsComponent;
import com.example.spaceflighter.game.components.HealthComponent;
import com.example.spaceflighter.game.components.MovingComponent;
import com.example.spaceflighter.game.components.PlayerComponent;

import java.util.ArrayList;
import java.util.List;

public class OutOfFieldSystem implements BaseSystem, BaseUpdateInterface {
    private ArrayList<Entity> fieldObjects = new ArrayList<>();
    private HealthComponent playerHealth;
    private float fieldWidth, fieldHeight;

    public OutOfFieldSystem(float fieldWidth, float fieldHeight) {
        this.fieldHeight = fieldHeight;
        this.fieldWidth = fieldWidth;
    }

    @Override
    public void update(long dT) {
        Object coordsComponent;
        for(Entity entity: fieldObjects) {
            coordsComponent = entity.getComponent(CoordsComponent.class.getName());
            PlayerComponent playerComponent = (PlayerComponent) entity.getComponent(PlayerComponent.class.getName());
            if(coordsComponent != null && playerComponent == null) {
                if(((CoordsComponent)coordsComponent).posX > fieldWidth || ((CoordsComponent)coordsComponent).posX < - fieldWidth) entity.kill();
                if(((CoordsComponent)coordsComponent).posY > fieldHeight ) entity.kill();
                if(((CoordsComponent)coordsComponent).posY < - fieldHeight) {
                    entity.kill();
                    if(entity.getId().equals("enemy") && playerHealth != null)  playerHealth.health -= 20;
                }
            }
        }
    }

    @Override
    public List<String> neededComponents() {
        List<String> output = new ArrayList<>();
        output.add(CoordsComponent.class.getName());
        return output;
    }

    @Override
    public void uploadEntities(ArrayList<Entity> entities) {
        fieldObjects = entities;
        for(Entity entity: entities) {
            if(entity.getComponent(PlayerComponent.class.getName()) != null) {
                playerHealth = (HealthComponent) entity.getComponent(HealthComponent.class.getName());
            }
        }
    }

    @Override
    public void addEntity(Entity entity) {
        if(entity.getComponent(MovingComponent.class.getName()) != null &&
                entity.getComponent(CoordsComponent.class.getName()) != null) {
            fieldObjects.add(entity);
            if(entity.getComponent(PlayerComponent.class.getName()) != null) {
                playerHealth = (HealthComponent) entity.getComponent(HealthComponent.class.getName());
            }
        }
    }

    @Override
    public void removeEntity(Entity entity) {
        fieldObjects.remove(entity);
        if(entity.getComponent(PlayerComponent.class.getName()) != null) {
            playerHealth = null;
        }
    }
}
