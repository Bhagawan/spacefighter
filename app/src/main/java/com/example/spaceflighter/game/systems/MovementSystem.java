package com.example.spaceflighter.game.systems;

import com.example.spaceflighter.game.Entity;
import com.example.spaceflighter.game.components.BoundsComponent;
import com.example.spaceflighter.game.components.CoordsComponent;
import com.example.spaceflighter.game.components.DrawableComponent;
import com.example.spaceflighter.game.components.MovingComponent;

import java.util.ArrayList;
import java.util.List;

public class MovementSystem implements BaseSystem, BaseUpdateInterface {
    private static final double PERFECT_DT = 16.67 * 2;
    ArrayList<Entity> movingObjects = new ArrayList<>();

    @Override
    public void update(long dT) {
        MovingComponent movingComponent;
        CoordsComponent coordsComponent;
        for(Entity entity: movingObjects) {
            movingComponent = (MovingComponent) entity.getComponent(MovingComponent.class.getName());
            coordsComponent = (CoordsComponent) entity.getComponent(CoordsComponent.class.getName());
            if(movingComponent != null && coordsComponent != null) {
                float dX = movingComponent.vectorX * (movingComponent.speed * (float)(dT / PERFECT_DT));
                float dY = movingComponent.vectorY * (movingComponent.speed * (float)(dT / PERFECT_DT));
                coordsComponent.posX += dX;
                coordsComponent.posY += dY;
                DrawableComponent drawableComponent = (DrawableComponent) entity.getComponent(DrawableComponent.class.getName());
                BoundsComponent boundsComponent = (BoundsComponent) entity.getComponent(BoundsComponent.class.getName());
                if(drawableComponent != null) drawableComponent.body.setPosition(coordsComponent.posX, coordsComponent.posY);
                if(boundsComponent != null) boundsComponent.moveToDelta(dX, dY);
            }
        }
    }

    @Override
    public List<String> neededComponents() {
        List<String> output = new ArrayList<>();
        output.add(MovingComponent.class.getName());
        output.add(CoordsComponent.class.getName());
        return output;
    }

    @Override
    public void uploadEntities(ArrayList<Entity> entities) {
        movingObjects = entities;
    }

    @Override
    public void addEntity(Entity entity) {
        if(entity.getComponent(MovingComponent.class.getName()) != null &&
                entity.getComponent(CoordsComponent.class.getName()) != null) movingObjects.add(entity);
    }

    @Override
    public void removeEntity(Entity entity) {
        movingObjects.remove(entity);
    }
}
